---
title: About
layout: page
permalink: "/about/"
nav_order: 1
---

![Off-Grid](/assets/images/off-grid_about.webp)


I am a technologist living off-grid in northern Vermont. We first moved off-grid in 2019. Along the way we have learned a lot! Surviving and thriving off-grid with a modern lifestyle isn't always straight forward. Often we need to come up with creative solutions to address a given problem or need. This webbook is a tool I put together to help document various methods and practices so we have a ready "handbook" (webbook) as a reference guide. If any of the information we've compiled is helpful for others, this site provides a bridge.
