---
title: Problems
layout: page
permalink: "/problems/"
nav_order: 2
---

# Problems

Problems may be an issue or simply a need. Some examples of common problems are: needing food, needing energy, winter gets cold, water (need it or need to get rid of it), etc.




## Heat

### More Heat is Needed

#### Climate
- [It's getting cold outside and I need more heat indoors.](/categories/energy/#generating-energy-heat)
- [ I have animals that need to stay warm through winter.](/categories/energy/#generating-energy-heat)
- [I need to keep my greenhouse above freezing for my plants all winter.](/categories/energy/#generating-energy-heat)
- [I need to keep my batteries or other appliance at a moderate temperature.](/categories/energy/#generating-energy-heat)


#### Water
- [I need warm or hot water for cleaning or bathing.](/categories/energy/#generating-energy-heat)
- [I need to prevent water from freezing](/categories/energy/#generating-energy-heat)
- [I want to heat water for warming something else like a room.](/categories/energy/#generating-energy-heat)


#### Food
- [I need to cook some food](/categories/energy/#generating-energy-heat)
- [I need to sterilize something.](/categories/energy/#generating-energy-heat)
- [I need to preserve some food.](/categories/energy/#generating-energy-heat)


#### Utility
- [I need to keep a pipe from freezing.](/categories/energy/#generating-energy-heat)
- [I need to remove a seized bolt.](/categories/energy/#generating-energy-heat)


<hr>

### Less Heat is Needed

#### Climate

#### Water


#### Food
- [I need to keep food cool.](/categories/food_preservation/#cool-storage)
- [I need to keep food cold.](/categories/food_preservation/#cold-storage)
- [I need to keep food frozen.](/categories/food_preservation/#freezing-storage)

#### Utility
- [I need to cool off an object.](/categories/chemical/#cooling) 

<hr>


## Water: A Need

### For Drinking
- [I need a good supply of water for drinking.](/categories/water/#well)

### For Utility
- [I need a supply of water for plants.](/categories/water/#rainwater)

### For Animals 
- [I need a supply of water for animals.](/categories/water/#rainwater)


<hr>

## Water: An issue

### Puddling or Collecting
- [I need to prevent water from pooling or puddling in the road or driveway.](/categories/water/#diverting)
- [I need to get rid of used water from things like doing dishes and bathing.](/categories/water/#graywater)


### Humidity
- [I need to remove some humidity from the air indoors.](/categories/water/#humidity)

### Mold and Mildew
- [I have mold growing somewhere indoors and I want to prevent it after properly cleaning.](/categories/water/#humidity)
