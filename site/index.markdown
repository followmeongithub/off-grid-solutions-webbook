---
layout: home
---


{: .warning }
Do **not** try to follow the methods and practices described within this site. Use personal 
discernment at all times. Failing to heed this warning could result in personal injury, death, 
or worse...

![Off-Grid](/assets/images/off-grid_winter.webp)


Welcome to the off-grid solutions webbook! Start with a problem or need you are facing 
to determine what known solutions might exist for this sort of problem. 
Click on the solution that fits best to better understand how the
 solution works.


Use the search bar at the top to search for content related to your problem.

**Example:**
> need heat

Or start at the [problems](/problems/) page and look for a problem or need that matches
your current problem or need. There are pages which describe various methods at a very
high level (.i.e. basic description), and there are pages dedicated to describing how to
utilize many of these methods in practice. The `how-to` content is minimal and incomplete
but offers a nudge in the right direction. Personal know-how, research, and experimentation
will be needed to fill in the gaps.
