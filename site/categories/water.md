---
title: Water
layout: page
parent: Categories
---

Here you can find all of our known methods for acquiring, collecting, storing,  diverting, and purifying water.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-zeta }
- TOC
{:toc}
</details>

# Acquiring
## Well
**Description:**
A hole dug (and often cased) to reach ground water which can include utilizing natural springs.

**Benefits:**
- Can provide a reliable source of clean water (assuming local water table not contaminated)


**Drawbacks:**
- Requires large special equipment and crew for drilling the well
- Commonly requires a well pump which are often high power demand

<hr>

# Collecting

## Rainwater
**Description:**
Collecting fallen rainwater from roofs and other surfaces.

**Benefits:**



**Drawbacks:**
- Should be considered non-potable

<hr>

## Humidity
**Description:**
Using various methods to condense and capture moisture from the air.

**Benefits:**



**Drawbacks:**


<hr>

# Storing


<hr>

# Diverting


<hr>

# Purifying
## For Drinking


<hr>

## For Graywater


<hr>
