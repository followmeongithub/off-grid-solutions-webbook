---
title: Categories
layout: page
permalink: "/categories/"
nav_order: 3
has_children: true
---

Categories are composed of the various high-level principals and methods we employ when solving any problem. Each of the methods are described in very broad terms and are not intended to be presecriptive. Use information found in the categories pages as hints of where to learn more next.
