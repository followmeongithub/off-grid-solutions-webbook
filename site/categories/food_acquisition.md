---
title: Food Acquisition
layout: page
parent: Categories
---

Here you can find methods for acquiring food.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-zeta }
- TOC
{:toc}
</details>
# Importing
Getting food from your grocery store or even your local CSA is a great example of importing food (to your home).

<hr>
# Farming

<hr>

## Plants
**Description:**
Using manual or automatic methods to assist and guide the growth of selected edible plants.

**Benefits:**
- Can be grown outdoors, in greenhouses, or indoors


**Drawbacks:**
- Often requires lots of manual labor
- Often months from planted seed to edible food
- Outdoor and greenhouse growing only works during warmer months

<hr>


## Animals
**Description:**
Keeping, feeding, and protecting animals either for food they produce such as eggs or milk, or for using as meat.

**Benefits:**
- Can be more reliable than hunting
- Can multiply (reproduction)
- "Free" entertainment


**Drawbacks:**
- Need to provide food...to your food
- Can be costly
- Requires additional expenses for fencing and housing and miscellaneous supplies

<hr>


# Foraging
**Description:**
Searching and locating  **safe** wild edible plant or fungus based edibles.

**Benefits:**
- Requires far less forethought


**Drawbacks:**
- Bounty may be scarce or non-existent
- Unless for critical survival, you should never take all of what you find because other wildlife likely thrive on this food source as well

<hr>


# Hunting
**Description:**
Aquisition of a wild animal for meat, typically using tools. Bow and arrow, gun, trap, and spears are examples of tools often used

**Benefits:**
- Can yield enough food to be worth preserving
- Traps can work while you rest


**Drawbacks:**
- Supplies like bullets and arrows are often not reusable (arrows have some reusability)
- Often requires a high amount of human energy
- Most areas require a license


<hr>



# Fishing
**Description:**
Aquisition of fish, typically using bait and fishing rod+line+hook. Nets are another option often used in bigger fishing waters (where legal).

**Benefits:**
- Low tech
- Low amount of human energy can be required
- Equipment is mostly all reusable


**Drawbacks:**
- Need a clean water source and reasonable access to it
- Need some equipment
- Most areas require a license

<hr>
