---
title: Food Preservation
layout: page
parent: Categories
---

Here you can find all of our known methods for preserving foods.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-zeta }
- TOC
{:toc}
</details>
# Cool Storage
**Description:**
One of the oldest forms of cool storage is a root cellar. Root cellars make use of earth insulation and geothermal cooling. They can also incorporate techniques such as passive thermal siphons.

**Benefits:**
- Low or no additional energy required
- Simple design (no special components required)


**Drawbacks:**
- Only particularly effective for certain foods, like root vegetables

<hr>

# Cold Storage
**Description:**
Often in the 32-40 degree (f) range, refrigerators are the most common method used for cold storage.

**Benefits:** 
- Can potentially benefit from lack of heat energy in winter by placing cold storage device in sheltered but non-climate controlled environments

**Drawbacks:**
- Requires special but very common equipment
- Requires nearly-constant energy supply
- Requires maintenance
- Commonly has a significant energy demand (but not as high as freezers!)

# Freezing Storage
**Description:**
Below 32 degrees (f) and preferrably at 0 degrees (f) or below, freezers are the common method for frozen storage.

**Benefits:**
- Can potentially benefit from lack of heat energy in winter by placing freezers in sheltered but non-climate controlled environments
- Many foods will store for a year or more in the freezer
- Ice cream

**Drawbacks:**
- Requires special but very common equipment
- Requires nearly-constant energy supply
- Requires maintenance
- Commonly has a significant energy demand

# Vacuum Sealing
**Description:**
Using purpose-built plastic bags or resealable containers like glass jars, you apply a vacuum to remove _most_ of the air, and then seal the container.

**Benefits:**
- Relatively simple and inexpensive
- If air can't get in, neither will most pests
- Can be used in tandem with most other preservation methods to help further extend freshness


**Drawbacks:**
- Requires special equipment
- Often needs to be used in tandem with another preservation method

<hr>

# Canning
**Description:**
Typically done with glass jars (though cans _can_ be used), an air tight seal is created. This locks out pests and prevents oxygen from entering which would promote bacteriail and mold growth.

**Benefits:**
- Food can last from 1-5 years
- Air and pest tight

**Drawbacks:**
- Can be contaminated with Botulinum or other becteria
- Glass jars break
- Seal may release or not fully seal causing food to spoil
- Food will often lose some nutritional value in the processing
- Special but simple equipment is necessary
- Not the most space-friendly


<hr>

# Pickling

**Description:**
Pickling uses anaerobic fermentation in a brine or vinegar, typically coupled with canning to preserve food.

**Benefits:**
- Same benefits as canning
- Pro-biotic (healthy)

**Drawbacks:**
- All (or most) of the drawbacks of canning
- Food can get mushy
- Changes the flavor
- Destroys some nutrients


<hr>

# Fermenting
**Description:**
Yeasts (wild or cultivated) are used to metabolize sugars in a solution. This process happens primarily without oxygen by way of filling the air cavity of your fermentation vessel with CO2 which is a byproduct of yeast metabolizing the sugars. A one-way valve is commonly used so that CO2 pressure doesn't build too high during fermentation as this can retard the growth of the yeast from a stressful environment.  The one-way vale also prevents other contaminents such as mold and bacteria from entering the fermentation vessel.

**Benefits:**
- Creates an environment for "good" bacteria that can be beneficial for gut (pro-biotic). Improved gut health can have numerous of other health benefits including immune and hart health

**Drawbacks:**
- Easily contaminated with Botulinum


<hr>

# Dehydrating
**Description:**
Typically heat and circulating air are applied to  food to extract much (but not all) of the moisture in an item.

**Benefits:**
- Only energy required is all up front, though some dehydrated things should still be kept cool
- Some drying methods utilize the sun for an even simpler design


**Drawbacks:**
- Oxidizes the food which damages some of the nutrients and causes visible browning

<hr>

# Freeze Drying
**Description:**
Freeze drying is a way of gently extracting water from foods (or other items) to leave a crispy-dry end product. Cooling, heating, and a vacuum are the mechanisms used to reach what is refered to as the "triple point" of water. This is the combination of temperature and pressure at which a given substance's state can transition between solid, liquid and gas. A vacuum is usually formed in the chmaber first, followed by cycles of freezing and heating. This allows the now frozen water to transition directly to gas (skipping the usual transition to water first) and then refreezes to the drum of the chamber. In the end this ice is defrosted and drained from the machine. Properly sealed and shaded freeze dried food can last for 25 years.

**Benefits:**
- Up to 25 year shelf life
- Does some of the least damage to nutrients in the food
- Only energy required is all up front


**Drawbacks:**
- Expensive specialized equipment
- High energy consumption
- Machine run time can be 24hrs or more
- Requires climate controlled environment (can't run in the garage in the middle of winter)

<hr>

# Replanting
**Description:**
This method is rarely applicable, but it can be useful to recover edible food from certain foods which may start growing in your cabinet or fridge. I'm not talking about mold! Common root vegetables such as potatoes, gralic, and onions may start sprouting. If they do, they are usually no longer good to eat. But rather than throwing away this "spoiled food", you can often replant it in soil and harvest the rewards later!

**Benefits:**
- Recovers otherwise wasted food


**Drawbacks:**
- May require additional care such as watering
- Results will vary
- There is still a "point of no return" (different per vegetable)
- Does not work with meat...

<hr>
