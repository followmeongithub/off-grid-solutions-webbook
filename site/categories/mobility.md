---
title: Mobility
layout: page
parent: Categories
---

Here you can find all of our known methods for moving anything from a person to a large object, as close as feet and as far as miles. These are primarily land-based methods as opposed to sea or air based methods.

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-zeta }
- TOC
{:toc}
</details>




# Combustion Motors: Wheeled Vehicles
**Description:**
Wheeled vehicles powered by a motor powered by little controlled explosions cause by fuel, heat, pressure, and often a spark.
Cars, trucks and 4-wheelers all fall under this category.

**Benefits:**
- Lots of power
- Very common


**Drawbacks:**
- Requires importing fuel, unless you have a fuel source such as wood-gas
- Toxic exhaust fumes

<hr>

# Combustion Motors: Tracked Vehicles
**Description:**
These include snowmobiles, trail groomers, excavators, 4-trackers, tanks, etc. Most of the same qualities as combustion egines driving wheels.
Primary differences are around terrain and efficiency. 

**Benefits:**
- Traction in sand, snow, and mud


**Drawbacks:**
- Typically creates more drag which means less efficient energy use

<hr>

	
# Electric Motors: Wheeled Vehicles
**Description:**
Wheeled vehicles powered by electric motors such as ebikes, electric golf carts, electric cars, etc.

**Benefits:**
- Electricity can be acquired or generated from many sources


**Drawbacks:**
- Often requires expensive, potentially dangerous (explosive) batteries
- Recharging takes more time than refueling

<hr>

# Pneumatic: Wheeled Vehicles
**Description:**
A wheeled vehicle which is powered by compressed gas such as air.

**Benefits:**
- No combustion/fuel required
- No electricty required (when operating)
- Relatively quiet operation
- Little to no excess heat is generated


**Drawbacks:**
- Equipment and designs are relatively scarce

<hr>

# Human Powered: Wheeled Vehicles
**Description:**
These include bicycles, carts, wheelbarrows, etc. Even things like roller skates fall under this category!

**Benefits:**
- Requires no fuel or charging
- Quiet operation


**Drawbacks:**
- Limited HP (human power)
- Requires food energy for the human

<hr>
	
	
# Force Multipliers
**Description:**
This category name is definitely my own clasification. "Force multipliers" come in many variations, but what they all share in common is the ability to provide a mechanical advantage. Examples include block an tackle, come along (cable winch) or rope-puller, gin pole, etc. 

**Benefits:**
- Can move large objects with human power
- Equipment is relatively inexpensive


**Drawbacks:**
- Can only move objects so far, so fast....not good for any real distance

<hr>
