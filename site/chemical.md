---
title: Chemical Reactions
layout: page
nav_exclude: true
---

On this page, you can find  methods for utilizing chemical reactions.


## Cooling
A simple example of a chemical reaction which results in an endothermic
reaction requires mixing the following two common household prodcuts:

1. Baking Soda (calcium bicarbonate)
2. Vinegar (acid)


## Heating
A simple example of a chemical reaction that results in exothermic 
reaction requires mixing the following two common household prodcuts:

1. Hydorgen Peroxide (H202)
2. Baking Powder (calcium bicarbonate + acid)


## Wood Gasification
A method of heating wood to a smoldering point that lets off flammable wood gas.
